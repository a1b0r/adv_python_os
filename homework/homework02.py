from collections import Counter

# Начнем с простейшей игры - крестики-нолики :)
# Реализуйте определение исхода игры на доске.
# Доска представлена кортежем.
# Крестик - 1, нолик - 0, пустые клетки обозначены None.
# Для визуализации я определил переменные с удобными именами.
# None я здесь именую _ (подчеркивание - валидное имя в Python).

X, O, _ = 1, 0, None

TEST_BOARD = (
    O, X, O,
    X, O, X,
    _, _, X
)

# Возможны четыре исхода, для них я тоже определил именованные константы.
O_WINS, X_WINS, UNDEFINED, DRAW = range(4)

# Первая подзадача и подсказка - реализуйте функцию,
# которая возвращает все возможные комбинации по 3 клетки в ряд:
# горизонтали, вертикали, диагонали. Таким образом эта часть
# задачи сводится к упражнению на слайсинг последовательности.
# Можете также попробовать реализовать как генератор.


def slice3(board):
    """
    Возвращает все возможные комбинации по 3 клетки в ряд:
    горизонтали, вертикали, диагонали.

    >>> from pprint import pprint
    >>> pprint(list(slice3((
    ... O, X, O,
    ... X, O, X,
    ... _, _, X
    ... ))))
    [(0, 1, 0),
     (1, 0, 1),
     (None, None, 1),
     (0, 1, None),
     (1, 0, None),
     (0, 1, 1),
     (0, 0, 1),
     (0, 0, None)]
    """
    for slice_of_3 in (board[:3], board[3:6], board[6:],
                       board[0::3], board[1:8:3], board[2::3],
                       board[0::4], board[2:8:2]):
        yield slice_of_3


def outcome(board):
    """
    Определение исхода.

    Допущение: доска может содержать только множество корректных состояний,
    которое могут получиться в результате справедливой игры.

    1. Когда на доске перечеркнуты первые 3 клетки - игра завершена.
    В этом случае вы должны определить, кто выиграл, и вернуть
    X_WINS или O_WINS.

    >>> outcome((
    ... X, X, O,
    ... O, X, _,
    ... O, _, X))
    1

    2. Для простоты мы пока предполагаем, что игра всегда доигрывается
    до конца. Пока есть пустые клетки и не перечеркнуты любые 3 -
    исход не определен - UNDEFINED.

    >>> outcome((
    ... X, X, O,
    ... O, O, X,
    ... _, _, X))
    2

    3. Если заполнены все клетки, и нет комбинации из трех смежных,
    выигрыш невозможен ни для кого, присуждаем ничью - DRAW

    >>> outcome((
    ... X, X, O,
    ... O, O, X,
    ... X, X, O))
    3
    """
    o_wins = 0
    x_wins = 0
    undefined = 0
    draw = 0

    zero_value = O
    cross_value = X

    for counter in slice3(board):
        if Counter(counter)[counter[0]] == 3:
            if counter[0] == zero_value:
                o_wins += 1
            elif counter[0] == cross_value:
                x_wins += 1

    if o_wins == x_wins == 0:
        if Counter(board)[zero_value] + Counter(board)[cross_value] == 9:
            draw += 1
        else:
            undefined += 1

    if o_wins and not x_wins:
        return O_WINS
    elif x_wins and not o_wins:
        return X_WINS
    elif draw:
        return DRAW
    elif undefined:
        return UNDEFINED


if __name__ == "__main__":
    import doctest
    doctest.testmod()

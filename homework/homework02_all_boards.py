from collections import Counter
from math import fabs

X, O, _ = 1, 0, None
O_WINS, X_WINS, UNDEFINED, DRAW = range(4)


def slice3(board):
    return board[:3], board[3:6], board[6:], board[0::3], board[1:8:3], board[2::3], board[0::4], board[2:8:2]


def outcome(board):
    o_wins = 0
    x_wins = 0
    undefined = 0
    draw = 0

    zero_value = O
    cross_value = X

    for counter in slice3(board):
        if Counter(counter)[counter[0]] == 3:
            if counter[0] == zero_value:
                o_wins += 1
            elif counter[0] == cross_value:
                x_wins += 1

    if o_wins == x_wins == 0:
        if Counter(board)[zero_value] + Counter(board)[cross_value] == 9:
            draw += 1
        else:
            undefined += 1

    if o_wins and not x_wins:
        return O_WINS
    elif x_wins and not o_wins:
        return X_WINS
    elif draw:
        return DRAW
    elif undefined:
        return UNDEFINED
    else:
        return None


class Board:
    __n_cell = 9

    def __init__(self,field=[None] * __n_cell):
        self.board = field
        self.is_valid = True

    def __str__(self):
        return "{},{},{}\n{},{},{}\n{},{},{}\n".format(self.board[0], self.board[1], self.board[2], self.board[3], self.board[4], self.board[5],self.board[6], self.board[7], self.board[8])

    def validate(self):
        dict_counter = Counter(self.board)
        if fabs(dict_counter[1] - dict_counter[0]) > 1:
            self.is_valid = False

    def get_all_board(self, counter=__n_cell - 1):
        if counter >= 0:
            self.board[counter] = _
            yield from self.get_all_board(counter - 1)
            self.board[counter] = X
            yield from self.get_all_board(counter - 1)
            self.board[counter] = O
            yield from self.get_all_board(counter - 1)
        else:
            yield Board(self.board)

    def get_results(self):
        results = outcome(self.board)
        if results is None:
            self.is_valid = False


class Game:

    def __init__(self):
        self.board = Board()


new_game = Game()

for current_board in new_game.board.get_all_board():
    current_board.validate()
    if current_board.is_valid:
        current_board.get_results()
        if current_board.is_valid:
            print(current_board)

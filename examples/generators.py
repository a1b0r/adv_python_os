def countdown(n):
    while n > 0:
        yield n
        n -= 1


def count():
    n = 0
    while True:
        yield n
        n += 1


def fake_fib():
    yield 1
    yield 1
    yield 2
    yield 3
    # raise StopIteration


# from itertools import isclice


def xcountdown(n):
    while n > 0:
        a = yield n
        if a is not None:
            n = a
        n -= 1


class Countdown:
    def __init__(self, n):
        self.n = n

    def __iter__(self):
        return self

    def __next__(self):
        if self.n <= 0:
            raise StopIteration()
        self.n -= 1
        return self.n


power2 = (x*x for x in range(10))

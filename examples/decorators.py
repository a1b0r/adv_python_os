from functools import wraps


def bounded(lower, upper):
    def inner(fn):
        @wraps(fn)
        def wrapper(*args):
            for arg in args:
                if lower > arg or arg > upper:
                    raise ValueError(
                        f'Arguments must be in range [{lower},{upper}]')
        return wrapper
    return inner


# xsub = bounded(0, 100)(add)


@bounded(0, 100)
def add(a, b):
    """Add numbers"""
    return a + b


@bounded(0, 100)
def sub(a, b):
    return a - b


def retry(num_attemps=10):
    def inner(fn):
        def wrapper(*args, **kwargs):
            for _ in range(num_attemps):
                try:
                    return fn(*args, **kwargs)
                except:
                    pass
        return wrapper
    return inner



import json


def to_json(fn):
    def wrapper(*args, **kwargs):
        result = fn(*args, kwargs)
        return json.dumps(result)
    return wrapper


@to_json
@retry(num_attemps=3)
def get_user(name):
    return {'username': 'Alex', 'email': 'a.tihonruk@gmail.com'}
